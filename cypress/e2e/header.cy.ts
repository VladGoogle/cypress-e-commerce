import LoginPage from "../support/page_object/login.page";
import HeaderPage from "../support/page_object/header.page";
import CartPage from "../support/page_object/cart.page";
import SidemenuPage from "../support/page_object/sidemenu.page";
import SearchResultsPage from "../support/page_object/searchResults.page";


describe('Header', ()=> {
    let testName
    before(() => {
        cy.visit('/')
    })

    beforeEach(async function () {
        testName = this.currentTest.title
    })

    it('should display header elems', () => {
        cy.waitForHeaderToBeDisplayed()
        HeaderPage.searchInput.percySnapshot(`Header--${testName}`)
    });

    it('should display login modal by clicking on login button', () => {
        HeaderPage.loginBtn.realClick()
        cy.waitForLoginPageToBeDisplayed()
        cy.percySnapshot(`Header--${testName}`)
    });

    it('should open cart by clicking on Cart button', () => {
        LoginPage.closeBtn.realClick()
        HeaderPage.cartBtn.realClick()
        CartPage.emptyCartMsg.should('be.visible')
        CartPage.closeBtn.should('be.visible')
        cy.percySnapshot(`Header--${testName}`)
    });

    it('should open side menu', () => {
        CartPage.closeBtn.realClick()
        HeaderPage.kebabMenu.realClick()
        SidemenuPage.sideMenuHeader.should('be.visible')
        cy.percySnapshot(`Header--${testName}`)
    });

    it('should return to the homepage by clicking on header logo',  ()=> {
        HeaderPage.headerLogo.realClick()

    });

    it('should correctly switch languages',  ()=> {
        HeaderPage.rusLangLink.realClick()
        HeaderPage.searchBtn.should('contain.text', 'Найти')
        HeaderPage.ukrLangLink.realClick()
        HeaderPage.searchBtn.should('contain.text', 'Знайти')
    });

})