import LoginPage from "../support/page_object/login.page";
import HeaderPage from "../support/page_object/header.page";
import CartPage from "../support/page_object/cart.page";
import SidemenuPage from "../support/page_object/sidemenu.page";
import SearchResultsPage from "../support/page_object/searchResults.page";


describe('Seacrh Results', ()=> {
    let testName

    before(() => {
        cy.visit('/')
    })

    beforeEach(async function () {
        testName = this.currentTest.title
    })

    it('should correctly search by search input', () => {
        HeaderPage.searchInput.type('Asus')
        HeaderPage.searchBtn.realClick()
        SearchResultsPage.productName.then($names => {
            const strings = [...$names].map(name => name.innerText.toLowerCase()).forEach((el) => {
                expect(el).to.have.string('asus')
            })
        })
    })

    it('should correctly sort by price ascending',  ()=> {
        SearchResultsPage.sortingDropdown.select('1: cheap')
        cy.wait(2500)
        SearchResultsPage.productPrice.then($items => {
            const strings = [...$items].map(item => parseInt(item.innerText.replace(/\s+/g, '')))
            expect(strings).to.deep.equal([...strings].sort())
        })
    });

    it('should correctly sort by price descending',  ()=> {
        SearchResultsPage.sortingDropdown.select('2: expensive')
        cy.wait(2000)
        SearchResultsPage.productPrice.then($items => {
            const strings = [...$items].map(item => parseInt(item.innerText.replace(/\s+/g, '')))
            expect(strings).to.deep.equal([...strings].sort(function(a,b){return a - b}).reverse())
        })
    });

    it('should show products in pointed price range',  ()=> {
        SearchResultsPage.minPriceInput.type(`{selectall}{backspace}20000`)
        SearchResultsPage.maxPriceInput.type(`{selectall}{backspace}30000`)
        SearchResultsPage.submitPriceFilterBtn.click()
        cy.wait(2000)
        SearchResultsPage.productPrice.then($items => {
            const strings = [...$items].map(item => parseInt(item.innerText.replace(/\s+/g, '')))
            strings.forEach((price)=>{
                expect(price).to.be.at.least(20000).and.to.be.at.most(30000)
            })
        })
    });

})