import exp = require("constants");
import {PostRequestInterface} from "../../interfaces/postRequest.interface";


describe('JSONPlaceholder', ()=> {

    let testName;
    let reqId;

    it('get Posts items - GET',  ()=> {

        cy.request('/posts').then((res)=>{
                expect(res.status).to.equal(200)
                expect(res.body.length).to.equal(100)
                expect(res.body[0].id).to.equal(1)
                reqId = res.body[0].id
            })
    });

    it('get one Post item - GET',  ()=> {
        cy.request(`/posts/${reqId}`).then((res)=>{
            expect(res.status).to.equal(200)
            expect(res.body.id).to.equal(1)
        })
    });

    it('create one Post item - POST',  ()=> {

        const reqBody: PostRequestInterface = {
            userId: 9,
            title: 'Simple title',
            body: 'This is a simple body'
        }

        cy.request(`POST`, `/posts`, reqBody).then((res)=>{
            expect(res.status).to.equal(201)
            expect(res.body).to.include.keys(`id`)
            expect(res.body.id).to.eq(101)
            reqId = res.body.id
        })
    })

    it('delete one Post item - POST',  ()=> {
        cy.request(`DELETE`, `/posts/599`).then((res)=>{
            expect(res.status).to.equal(404)
        })
    });

    it('update one Post item - PUT',  ()=> {
        const reqBody: PostRequestInterface = {
            userId: 1993939,
            title: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
            body: 'AAATTTTTTTTTTTTTTTTTTTTT'
        }

        cy.request(`PUT`, `/posts/1`, reqBody).then((res)=>{
            expect(res.status).to.equal(200)
        })
    });
})