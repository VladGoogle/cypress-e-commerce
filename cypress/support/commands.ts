// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
import LoginPage from "./page_object/login.page";
import HeaderPage from "./page_object/header.page";

Cypress.Commands.add('waitUntilUserButtonIsPresent', () => {
    let remainingAttempts = 30;
    let $elem = Cypress.$("button:contains('00005100579')");
    if ($elem.length) {
        return $elem
    }

    if (--remainingAttempts) {
        cy.log('Button not found yet. Remaining attempts: ' + remainingAttempts);
        cy.reload();
        return cy.wait(1000).then(() => {
            return cy.waitUntilUserButtonIsPresent();
        });
    }
    throw Error('Button was not found.');
})

Cypress.Commands.add('waitForHeaderToBeDisplayed', () => {
    HeaderPage.headerLogo.should('be.visible')
    HeaderPage.searchInput.should('be.visible')
    HeaderPage.searchBtn.should('be.visible')
    HeaderPage.microBtn.should('be.visible')
    HeaderPage.loginBtn.should('be.visible')
    HeaderPage.cartBtn.should('be.visible')
    HeaderPage.catalogMenu.should('be.visible')
    HeaderPage.kebabMenu.should('be.visible')
})

Cypress.Commands.add('waitForLoginPageToBeDisplayed', () => {
    LoginPage.emailInput.should('be.visible')
    LoginPage.passwordInput.should('be.visible')
    LoginPage.hidePasswordBtn.should('be.visible')
    LoginPage.rememberCheckbox.should('be.visible')
    LoginPage.submitBtn.should('be.visible')
    LoginPage.registerLink.should('be.visible')
    LoginPage.forgotPasswordLink.should('be.visible')
    LoginPage.facebookLoginBtn.should('be.visible')
    LoginPage.googleLoginBtn.should('be.visible')
    LoginPage.closeBtn.should('be.visible')
})


Cypress.Commands.add('swipe', { prevSubject: 'element'}, (subject: JQuery<HTMLElement>,x: number,  options, y?:number,) => {
    const htmlElement = subject.get(0)
    const elRect = htmlElement.getBoundingClientRect()
    const elCenterX = elRect.left + (elRect.width / 2)
    const elCenterY = elRect.top + (elRect.height / 2)

    const mouseDown = new MouseEvent('mousedown', {
        bubbles: true,
        cancelable: true,
        view: window,
        clientX: elCenterX,
        clientY: elCenterY,
        screenX: window.screenX + elCenterX,
        screenY: window.screenY + elCenterY
    })

    const mouseMove = new MouseEvent('mousemove', {
        bubbles: true,
        cancelable: true,
        view: window,
        clientX: elCenterX + x,
        clientY:  elCenterY + y,
        screenX: window.screenX + elCenterX + x,
        screenY: window.screenY + elCenterY + y
    })

    const mouseUp = new MouseEvent('mouseup', {
        bubbles: true,
        cancelable: true,
        view: window,
        clientX: elCenterX + x,
        clientY: elCenterY + y,
        screenX: window.screenX + elCenterX + x,
        screenY: window.screenY + elCenterY + y
    })

    htmlElement.dispatchEvent(mouseDown)
    htmlElement.dispatchEvent(mouseMove)
    htmlElement.dispatchEvent(mouseUp)
})

