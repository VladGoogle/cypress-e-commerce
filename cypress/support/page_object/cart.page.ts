class CartPage {

    get emptyCartMsg() {
        return cy.get('[data-testid="empty-cart"]')
    }

    get closeBtn() {
        return cy.get('.modal__close')
    }
}

export default new CartPage()