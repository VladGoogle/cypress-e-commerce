class HeaderPage {

    get headerLogo() {
        return cy.get('.header__logo')
    }

    get searchInput() {
        return cy.get('input[class^="search-form__input"]')
    }

    get microBtn() {
        return cy.get('button.search-form__microphone')
    }

    get searchBtn() {
        return cy.get('button.search-form__submit')
    }

    get loginBtn() {
        return cy.xpath('/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/ul/li[3]/rz-user/button')
    }

    get cartBtn() {
        return cy.xpath('/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/ul/li[7]/rz-cart/button')
    }

    get catalogMenu() {
        return cy.get('#fat-menu')
    }

    get kebabMenu() {
        return cy.xpath('/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/rz-mobile-user-menu/button')
    }

    get rusLangLink() {
        return cy.xpath('/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/ul/li[1]/rz-lang/ul/li[1]/a')
    }

    get ukrLangLink() {
        return cy.xpath('/html/body/app-root/div/div/rz-header/rz-main-header/header/div/div/ul/li[1]/rz-lang/ul/li[2]/a')
    }


}

export default new HeaderPage()