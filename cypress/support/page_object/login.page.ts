class LoginPage {

     get emailInput() {
          return cy.get('#auth_email')
     }

     get passwordInput() {
          return cy.get('#auth_pass')
     }

     get hidePasswordBtn() {
          return cy.get('button[class*="form__toggle-password"]')
     }

     get rememberCheckbox() {
          return cy.get('.auth-modal__remember-checkbox')
     }

     get submitBtn() {
          return cy.get('button[class*="auth-modal__submit"]')
     }

     get registerLink() {
          return cy.get('button[class*="auth-modal__register-link"]')
     }

     get facebookLoginBtn() {
          return cy.get('button[class*="auth-modal__social-button_type_facebook"]')
     }

     get googleLoginBtn() {
          return cy.xpath('/html/body/app-root/rz-single-modal-window/div[3]/div[2]/rz-user-identification/rz-auth/div/div/div/rz-social-auth/button[2]')
     }

     get forgotPasswordLink() {
          return cy.get('a[class*="auth-modal__restore-link"]')
     }

     get closeBtn() {
          return cy.get('.modal__close')
     }
}

export default new LoginPage()