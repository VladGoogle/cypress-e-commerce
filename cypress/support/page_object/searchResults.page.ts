
class SearchResultsPage {
    get productName() {
        return cy.get('.goods-tile__title')
    }

    get productPrice() {
        return cy.get('.goods-tile__price-value')
    }

    get sortingDropdown() {
        return cy.xpath('/html/body/app-root/div/div/rz-search/rz-catalog/div/div[1]/div/rz-sort/select')
    }

    get bigTileViewBtn() {
        return cy.xpath('/html/body/app-root/div/div/rz-search/rz-catalog/div/div[1]/div/rz-view-switch/div/button[1]')
    }

    get smallTileViewBtn() {
        return cy.xpath('/html/body/app-root/div/div/rz-search/rz-catalog/div/div[1]/div/rz-view-switch/div/button[2]')
    }

    get minPriceInput() {
        return cy.get('input[formcontrolname="min"]')
    }

    get maxPriceInput() {
        return cy.get('input[formcontrolname="max"]')
    }

    get submitPriceFilterBtn() {
        return cy.get('.slider-filter__inner > .button')
    }

    get leftSliderButton() {
        return cy.get('button.rz-slider__range-button_type_left')
    }

    get rightSliderButton() {
        return cy.get('button.rz-slider__range-button_type_right')
    }
}

export default new SearchResultsPage()