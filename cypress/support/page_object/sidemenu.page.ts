
class SideMenuPage {

    get sideMenuHeader() {
        return cy.get('div.side-menu__header')
    }

    get sideMenuLoginBtn() {
        return cy.xpath('//*[@id="cdk-overlay-1"]/nav/div/div[2]/div/div/div[2]/div/button[1]')
    }

    get sideMenuRegisterBtn() {
        return cy.xpath('//*[@id="cdk-overlay-1"]/nav/div/div[2]/div/div/div[2]/div/button[2]')
    }

    get cityToggle() {
        return cy.get('button.city-toggle')
    }

    get rusLangBtn() {
        return cy.xpath('//*[@id="cdk-overlay-1"]/nav/div/div[2]/ul[2]/li[1]/div[1]/rz-lang/ul/li[1]/a')
    }

    get ukrLangBtn() {
        return cy.xpath('//*[@id="cdk-overlay-0"]/nav/div/div[2]/ul[2]/li[1]/div[1]/rz-lang/ul/li[2]/a')
    }

    get closeBtn() {
        return cy.get('button.side-menu__close')
    }
}

export default new SideMenuPage()