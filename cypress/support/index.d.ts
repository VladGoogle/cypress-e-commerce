export {};

declare global {
    namespace Cypress {
        interface Chainable<Subject> {
            waitForLoginPageToBeDisplayed(): void,
            waitForHeaderToBeDisplayed(): void
            waitForWomenTabToBeDisplayed(): void
            waitUntilUserButtonIsPresent():void
            waitForLoginModalToBeDisplayed(): void
            swipe(x: number, y?:number): void
        }
    }
}