export interface PostRequestInterface {
    userId: number,
    name?: string,
    surname?: string,
    title: string,
    body: string
}