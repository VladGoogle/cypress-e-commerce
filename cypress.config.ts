import { defineConfig } from 'cypress'

export default defineConfig({
  viewportHeight: 768,
  viewportWidth: 1366,
  fixturesFolder: false,
  e2e: {
    defaultCommandTimeout: 20000,
    requestTimeout: 60000,
    includeShadowDom: true,
    baseUrl: 'https://jsonplaceholder.typicode.com'
  },
})
